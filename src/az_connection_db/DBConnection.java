package az_connection_db;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

import aa_config.Config_project;

public class DBConnection {
	 
    static Connection connection=null;
    public static Connection getConnection(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
 
            
            if(connection==null)
            {
            	connection=(Connection) DriverManager.getConnection(
                		Config_project.DATABASE_ADDRESS,           		
                		Config_project.DATABASE_USER_NAME,            		
                		Config_project.DATABASE_PASSWORD);
            }
            
            
            
            System.out.println("Connected");
            
        } catch (Exception e) {
        }
        
        return connection;
    }
    
    public static void main(String[] args) {
        getConnection();
    }
}
