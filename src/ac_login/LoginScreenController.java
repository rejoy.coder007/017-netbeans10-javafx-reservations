package ac_login;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import com.mysql.jdbc.PreparedStatement;

import aa_config.Config_project;
import ab_main.ControlledScreen;
import ab_main.Main;
import ab_main.ScreensController;
import az_connection_db.DBConnection;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.controlsfx.control.Notifications;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class LoginScreenController implements Initializable, ControlledScreen {

    ScreensController myController;
    

    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO Auto-generated method stub
        System.out.println("Inside Login");
    }
 
    
    public void Init_Windows(HashMap<String, String> parameters) {
        
        
        
        
       
   
         double decorationWidth = Config_project.START_WIDTH - Main.scene.getWidth();
         double decorationHeight = Config_project.START_HEIGHT  - Main.scene.getHeight();
       
          Main.stage.setWidth(Config_project.LOGIN_WIDTH+decorationWidth); 
           Main.stage.setHeight(Config_project.LOGIN_HEIGHT+decorationHeight);
//           stackepane.setAlignment(Pos.CENTER);
         
    }

}
