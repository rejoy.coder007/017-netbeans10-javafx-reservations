package ab_main;
	
 

import aa_config.Config_project;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;

 
public class Main extends Application {
    
    public static String screen1ID = "MainScreen";
    public static String screen1File = "aa_Main.fxml";
    public static float  screen1Time = 100;
    
  
    public static String screen2ID = "LoginScreen";
    public static String screen2File = "ab_Login.fxml";
    public static float  screen2Time = 3000;
    
    
    public static Stage stage;
    public static Scene scene;
    
    /*
    public static String screen3ID = "screen3";
    public static String screen3File = "Screen3.fxml";
    */
    
    @Override
    public void start(Stage primaryStage) {
        
        ScreensController mainContainer = new ScreensController();
        
        Main.stage=primaryStage;
        
        mainContainer.loadScreen(Main.screen1ID, Main.screen1File);
        mainContainer.loadScreen(Main.screen2ID, Main.screen2File);
        
 
       // mainContainer.setScreen(Main.screen1ID,Main.screen1Time);
       

        
        
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Main.scene = new Scene(root,Config_project.START_WIDTH,Config_project.START_HEIGHT);
        
        
        
        primaryStage.setScene( Main.scene);
        
        
        
        primaryStage.getIcons().add(new Image("zz_Images/aa_icon.png"));
		primaryStage.setTitle(Config_project.COMPANY_NAME);
		
		if(!Config_project.TEST)
	 	primaryStage.initStyle(StageStyle.TRANSPARENT);

         // mainContainer.setScreen(Main.screen1ID,Main.screen1Time,null);
           mainContainer.setScreen(Main.screen2ID,Main.screen2Time,null);
          primaryStage.show();
        System.out.println(mainContainer.screenCollection);
    }

   
    public static void main(String[] args) {
        launch(args);
    }
}
