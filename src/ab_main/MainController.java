package ab_main;

import java.net.URL;
import java.util.ResourceBundle;

import aa_config.Config_project;
import java.util.HashMap;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class MainController implements Initializable, ControlledScreen {

    ScreensController myController;

    @FXML
    private ImageView imageStartup;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public void setScreenParent(ScreensController screenParent) {
        myController = screenParent;
    }

    public void Init_Windows(HashMap<String, String> parameters) {

        FadeTransition fadeTransition = new FadeTransition();
        fadeTransition.setDuration(Duration.millis(Config_project.START_WINDOW_DURATION_IMAGE));

        fadeTransition.setFromValue(Config_project.START_WINDOW_FROM);
        fadeTransition.setToValue(Config_project.START_WINDOW_TRANSITION_TO);

        fadeTransition.setOnFinished(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                myController.setScreen(Main.screen2ID,Main.screen2Time,null);

            }
        }
        );

        fadeTransition.setNode(imageStartup);
        fadeTransition.play();

    }

}
